// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCN5FfxABVOlfZZwXSpO7n2xEXCp2-Fup0",
    authDomain: "pharmacad-66ed8.firebaseapp.com",
    databaseURL: "https://pharmacad-66ed8.firebaseio.com",
    projectId: "pharmacad-66ed8",
    storageBucket: "pharmacad-66ed8.appspot.com",
    messagingSenderId: "974601836072",
    appId: "1:974601836072:web:94f1772cbefa96b3b7071d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
