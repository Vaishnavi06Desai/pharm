import { BrowserModule, HammerModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Injectable} from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AngularFireModule } from '@angular/fire';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { AuthService } from './auth/auth.service';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';

import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {MatNativeDateModule} from '@angular/material/core';
import {MaterialsModule} from './materials/materials.module';
import { GpatComponent } from './gpat/gpat.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FreeVidsService } from './free-vids/free-vids.service';
import { TopicsComponent } from './free-vids/topics/topics.component';
import { ChaptersComponent } from './free-vids/chapters/chapters.component';
import { VideoComponent } from './free-vids/video/video.component';


//import 'hammerjs';
import * as Hammer from 'hammerjs';
import { MemchartsComponent } from './memcharts/memcharts.component';
import { BooksComponent } from './books/books/books.component';
import { BookComponent } from './books/book/book.component';
import { BooksService } from './books/books.service';
import { SubjectsComponent } from './gpat/subjects/subjects.component';
import { McqsComponent } from './gpat/subjects/mcqs/mcqs.component';
import { NotesComponent } from './gpat/subjects/notes/notes.component';
import { VideosComponent } from './gpat/subjects/videos/videos.component';
import { NoteComponent } from './gpat/subjects/notes/note/note.component';
import { McqComponent } from './gpat/subjects/mcqs/mcq/mcq.component';
import { GpattopicsComponent } from './gpat/subjects/videos/gpattopics/gpattopics.component';
import { GpatvideoComponent } from './gpat/subjects/videos/gpattopics/gpatvideo/gpatvideo.component';
import { SemesterComponent } from './semester/semester.component';
import { SemSubjectsComponent } from './semester/sem-subjects/sem-subjects.component';
import { SemSubOptsComponent } from './semester/sem-subjects/sem-sub-opts/sem-sub-opts.component';
import { SemPastPapersComponent } from './semester/sem-subjects/sem-sub-opts/sem-past-papers/sem-past-papers.component';
import { SemNotesComponent } from './semester/sem-subjects/sem-sub-opts/sem-notes/sem-notes.component';
import { SemVideosComponent } from './semester/sem-subjects/sem-sub-opts/sem-videos/sem-videos.component';
import { SemTopicsComponent } from './semester/sem-subjects/sem-sub-opts/sem-videos/sem-topics/sem-topics.component';
import { SemVideoComponent } from './semester/sem-subjects/sem-sub-opts/sem-videos/sem-topics/sem-video/sem-video.component';
import { SemNoteComponent } from './semester/sem-subjects/sem-sub-opts/sem-notes/sem-notes/sem-note/sem-note.component';
import { SemPastPaperComponent } from './semester/sem-subjects/sem-sub-opts/sem-past-papers/sem-past-paper/sem-past-paper.component';
import { TestSeriesComponent } from './test-series/test-series.component';
import { ProfileComponent } from './profile/profile.component';
import { SupportComponent } from './support/support.component';
import { CertificationComponent } from './certification/certification.component';
import { FeeComponent } from './fee/fee.component';
import { FeesComponent } from './fees/fees.component';
import { PaydetComponent } from './paydet/paydet.component';
import { PaydetsComponent } from './paydets/paydets.component';



// import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

@Injectable({providedIn: 'root'})
export class HammerConfig extends HammerGestureConfig {
  buildHammer(element: HTMLElement) {
    let mc = new Hammer(element, {
      touchAction: "pan-y",
    });
    return mc;
  }
} 

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    HomeComponent,
    NavbarComponent,
    GpatComponent,
    TopicsComponent,
    ChaptersComponent,
    VideoComponent,
    MemchartsComponent,
    BooksComponent,
    BookComponent,
    SubjectsComponent,
    McqsComponent,
    NotesComponent,
    VideosComponent,
    NoteComponent,
    McqComponent,
    GpattopicsComponent,
    GpatvideoComponent,
    SemesterComponent,
    SemSubjectsComponent,
    SemSubOptsComponent,
    SemPastPapersComponent,
    SemNotesComponent,
    SemVideosComponent,
    SemTopicsComponent,
    SemVideoComponent,
    SemNoteComponent,
    SemPastPaperComponent,
    TestSeriesComponent,
    ProfileComponent,
    SupportComponent,
    CertificationComponent,
    FeeComponent,
    FeesComponent,
    PaydetComponent,
    PaydetsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'Pharmacad'),
    AngularFirestoreModule, // Only required for database features
    AngularFireAuthModule, // Only required for auth features,
    AngularFireStorageModule,
    
    NgbModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MaterialsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

    HammerModule,

    //Location,
    //PathLocationStrategy
    //Hammer,

  ],
  providers: [
    AuthService, 
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' }}, 
    FreeVidsService,
     {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig,
    },
    BooksService
  ],
    
  bootstrap: [AppComponent]
})
export class AppModule { }
