import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemSubOptsComponent } from './sem-sub-opts.component';

describe('SemSubOptsComponent', () => {
  let component: SemSubOptsComponent;
  let fixture: ComponentFixture<SemSubOptsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemSubOptsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemSubOptsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
