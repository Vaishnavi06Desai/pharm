import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { SemNotesService } from './sem-notes.service';

@Component({
  selector: 'app-sem-notes',
  templateUrl: './sem-notes.component.html',
  styleUrls: ['./sem-notes.component.scss']
})
export class SemNotesComponent implements OnInit {

  subject;
  chapters;
  opened: boolean;  
  user: firebase.User;

  constructor(private ns: SemNotesService,
    private router: Router,
    private db: AngularFirestore,
    private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getchapters();
  }

  logout() {
    this.auth.logout();
  }

  getchapters()
  {
    this.subject =  this.ns.getsubject();
    this.ns.getchapters().subscribe(res => {this.chapters = res;});
  }

  next(chapter)
  {
    this.ns.setchapter(chapter);
    console.log('Hi');
    this.router.navigate(['/semester/semsub/options/notes/note']);
  }


}
