import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemNotesComponent } from './sem-notes.component';

describe('SemNotesComponent', () => {
  let component: SemNotesComponent;
  let fixture: ComponentFixture<SemNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
