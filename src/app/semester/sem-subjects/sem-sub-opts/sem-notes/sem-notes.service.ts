import { Injectable } from '@angular/core';
import { SemSubjectsService } from '../../sem-subjects.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class SemNotesService {

  subject;
  chapter;
  semester;

  constructor(private sss: SemSubjectsService,
    private db: AngularFirestore) { }

  getsubject()
  {
    this.subject = this.sss.getsubject();
    return this.subject;
  }

  getsem()
  {
    this.semester = this.sss.getsemester();
  }

  getchapters()
  {
    this.getsubject();
    this.getsem();
    return this.db.collection('Semester').doc(this.semester).collection('Subjects').doc(this.subject).collection('Notes').snapshotChanges(); 
  }

  setchapter(chapter)
  {
    this.chapter = chapter;
  }

  getnote()
  {
    return this.chapter; 
  }
}
