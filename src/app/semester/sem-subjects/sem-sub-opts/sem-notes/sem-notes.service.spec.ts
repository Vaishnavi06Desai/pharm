import { TestBed } from '@angular/core/testing';

import { SemNotesService } from './sem-notes.service';

describe('SemNotesService', () => {
  let service: SemNotesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SemNotesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
