import { Component, OnInit } from '@angular/core';
import { SemNotesService } from '../../sem-notes.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-sem-note',
  templateUrl: './sem-note.component.html',
  styleUrls: ['./sem-note.component.scss']
})
export class SemNoteComponent implements OnInit {

  note;

  constructor(private ns: SemNotesService,
              public sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.getnote();
  }

  getnote()
  {
    this.note = this.ns.getnote();
  }
}
