import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemNoteComponent } from './sem-note.component';

describe('SemNoteComponent', () => {
  let component: SemNoteComponent;
  let fixture: ComponentFixture<SemNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
