import { TestBed } from '@angular/core/testing';

import { SemPapersService } from './sem-papers.service';

describe('SemPapersService', () => {
  let service: SemPapersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SemPapersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
