import { Component, OnInit } from '@angular/core';
import { SemPapersService } from '../sem-papers.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-sem-past-paper',
  templateUrl: './sem-past-paper.component.html',
  styleUrls: ['./sem-past-paper.component.scss']
})
export class SemPastPaperComponent implements OnInit {

  paper;

  constructor(private ps: SemPapersService,
              public sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.getpaper();
  }

  getpaper()
  {
    this.paper = this.ps.getpaper();
  }

}
