import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemPastPaperComponent } from './sem-past-paper.component';

describe('SemPastPaperComponent', () => {
  let component: SemPastPaperComponent;
  let fixture: ComponentFixture<SemPastPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemPastPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemPastPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
