import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemPastPapersComponent } from './sem-past-papers.component';

describe('SemPastPapersComponent', () => {
  let component: SemPastPapersComponent;
  let fixture: ComponentFixture<SemPastPapersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemPastPapersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemPastPapersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
