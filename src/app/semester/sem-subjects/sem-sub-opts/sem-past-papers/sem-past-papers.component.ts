import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from 'src/app/auth/auth.service';
import { SemPapersService } from './sem-papers.service';

@Component({
  selector: 'app-sem-past-papers',
  templateUrl: './sem-past-papers.component.html',
  styleUrls: ['./sem-past-papers.component.scss']
})
export class SemPastPapersComponent implements OnInit {

  subject;
  chapters;
  opened: boolean;  
  user: firebase.User;

  constructor(private ps: SemPapersService,
    private router: Router,
    private db: AngularFirestore,
    private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getchapters();
  }

  logout() {
    this.auth.logout();
  }

  getchapters()
  {
    this.subject =  this.ps.getsubject();
    this.ps.getchapters().subscribe(res => {this.chapters = res;});
  }

  next(chapter)
  {
    this.ps.setchapter(chapter);
    console.log('Hi');
    this.router.navigate(['semester/semsub/options/papers/paper']);
  }

}
