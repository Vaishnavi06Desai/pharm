import { Injectable } from '@angular/core';
import { SemSubjectsService } from '../../sem-subjects.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class SemPapersService {

  subject;
  chapter;
  semester;

  constructor(private sss: SemSubjectsService,
    private db: AngularFirestore) { }

  getsubject()
  {
    this.subject = this.sss.getsubject();
    return this.subject;
  }

  getsem()
  {
    this.semester = this.sss.getsemester();
  }

  getchapters()
  {
    this.getsubject();
    this.getsem();
    return this.db.collection('Semester').doc(this.semester).collection('Subjects').doc(this.subject).collection('Papers').snapshotChanges();
  }

  setchapter(chapter)
  {
    this.chapter = chapter;
  }

  getpaper()
  {
    return this.chapter; 
  }
}

