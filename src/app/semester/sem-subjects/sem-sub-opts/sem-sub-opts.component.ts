import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { SemSubjectsService } from '../sem-subjects.service';

@Component({
  selector: 'app-sem-sub-opts',
  templateUrl: './sem-sub-opts.component.html',
  styleUrls: ['./sem-sub-opts.component.scss']
})
export class SemSubOptsComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  subject: String = "none";
  name;

  constructor(private db: AngularFirestore,
    private auth: AuthService,
    private sss: SemSubjectsService,
    private router: Router) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getname();
  }

  logout() {
    this.auth.logout();
  }

  papers()
  {
    this.router.navigate(['/semester/semsub/options/papers']);
  }

  notes()
  {
    this.router.navigate(['/semester/semsub/options/notes']);
  }

  videos()
  {
    this.router.navigate(['/semester/semsub/options/videos']);
  }

  getname()
  {
    this.sss.getname().subscribe(res => {this.name = res;});
  }


}
