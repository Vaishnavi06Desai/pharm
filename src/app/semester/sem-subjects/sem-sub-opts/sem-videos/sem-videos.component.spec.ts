import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemVideosComponent } from './sem-videos.component';

describe('SemVideosComponent', () => {
  let component: SemVideosComponent;
  let fixture: ComponentFixture<SemVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
