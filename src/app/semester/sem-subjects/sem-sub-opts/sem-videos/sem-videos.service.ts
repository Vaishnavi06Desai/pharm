import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { SemSubjectsService } from '../../sem-subjects.service';

@Injectable({
  providedIn: 'root'
})
export class SemVideosService {

  subject: string;
  chapter;
  topic;
  semester;

  constructor(private db: AngularFirestore,
              private sss: SemSubjectsService) { }

  getsubject()
  {
    this.subject = this.sss.getsubject();
  }

  getsem()
  {
    this.semester = this.sss.getsemester();
  }

  getchapters()
  {
    this.getsubject();
    this.getsem();
    return this.db.collection('Semester').doc(this.semester).collection('Subjects').doc(this.subject).collection('Videos').snapshotChanges();
  }

  setchapter(chapter)
  {
    this.chapter = chapter;
  }

  gettopics()
  {
    return this.db.collection('Semester').doc(this.semester).collection('Subjects').doc(this.subject).collection('Videos').doc(this.chapter.payload.doc.id).collection('Topics').snapshotChanges();
  }

  settopic(topic)
  {
    this.topic = topic;
  }

  gettopic()
  {
    return this.topic;
  }
}
