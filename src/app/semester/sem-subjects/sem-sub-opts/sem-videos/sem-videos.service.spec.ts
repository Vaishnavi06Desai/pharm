import { TestBed } from '@angular/core/testing';

import { SemVideosService } from './sem-videos.service';

describe('SemVideosService', () => {
  let service: SemVideosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SemVideosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
