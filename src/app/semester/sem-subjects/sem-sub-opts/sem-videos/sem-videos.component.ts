import { Component, OnInit } from '@angular/core';
import { SemVideosService } from './sem-videos.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-sem-videos',
  templateUrl: './sem-videos.component.html',
  styleUrls: ['./sem-videos.component.scss']
})
export class SemVideosComponent implements OnInit {

  chapters;
  opened: boolean;  
  user: firebase.User;

  constructor( private auth: AuthService,
    private vs: SemVideosService,
    private router: Router) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getchapters();
  }

  getchapters()
  {
    this.vs.getchapters().subscribe(res => (this.chapters = res));
  }

  logout() {
    this.auth.logout();
  }

  next(chapter)
  {
    this.vs.setchapter(chapter);
    this.router.navigate(['/semester/semsub/options/videos/topics']);
  }

}
