import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemTopicsComponent } from './sem-topics.component';

describe('SemTopicsComponent', () => {
  let component: SemTopicsComponent;
  let fixture: ComponentFixture<SemTopicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemTopicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemTopicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
