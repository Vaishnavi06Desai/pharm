import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { SemVideosService } from '../../sem-videos.service';

@Component({
  selector: 'app-sem-video',
  templateUrl: './sem-video.component.html',
  styleUrls: ['./sem-video.component.scss']
})
export class SemVideoComponent implements OnInit {

  video: any;

  constructor(private db: AngularFirestore,
    private sv: SemVideosService,
    private router: Router) { }

    ngOnInit(): void {
      this.getvid();
    }
  
    getvid(){
      this.video = this.sv.gettopic();
    }
  
}
