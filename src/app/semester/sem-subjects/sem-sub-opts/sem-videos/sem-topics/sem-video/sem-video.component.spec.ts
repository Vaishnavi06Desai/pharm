import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemVideoComponent } from './sem-video.component';

describe('SemVideoComponent', () => {
  let component: SemVideoComponent;
  let fixture: ComponentFixture<SemVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
