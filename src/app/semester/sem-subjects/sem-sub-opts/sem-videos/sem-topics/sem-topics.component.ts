import { Component, OnInit } from '@angular/core';
import { SemVideosService } from '../sem-videos.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-sem-topics',
  templateUrl: './sem-topics.component.html',
  styleUrls: ['./sem-topics.component.scss']
})
export class SemTopicsComponent implements OnInit {

  topics;
  opened: boolean;  
  user: firebase.User;

  constructor(private auth: AuthService,
    private vs: SemVideosService,
    private router: Router) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.gettopics();
  }

  gettopics(){
    this.vs.gettopics().subscribe(res => (this.topics = res));
  }

  logout() {
    this.auth.logout();
  }

  playvid(topic)
  {
    this.vs.settopic(topic);
    this.router.navigate(['/semester/semsub/options/videos/topics/video']);
  }

}
