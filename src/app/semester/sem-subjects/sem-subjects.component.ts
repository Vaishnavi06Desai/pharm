import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { SemSubjectsService } from './sem-subjects.service';

@Component({
  selector: 'app-sem-subjects',
  templateUrl: './sem-subjects.component.html',
  styleUrls: ['./sem-subjects.component.scss']
})
export class SemSubjectsComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  subjects;
  semester;

  constructor(private auth: AuthService,
    private router: Router,
    private sss: SemSubjectsService) { }

    ngOnInit(): void {
      this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
      })
      this.getsem();
      this.getsubjects();
    }

    logout() {
      this.auth.logout();
    }
  
    getsem()
    {
      this.semester =  this.sss.getsemester();
    }
    getsubjects()
    {
      this.sss.getsubjects().subscribe(res => {this.subjects = res});
    }
    setsubject(subject: string)
    {
      this.sss.setsubject(subject);
      this.router.navigate(['/semester/semsub/options']);
    }

}
