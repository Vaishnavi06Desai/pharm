import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { SemesterService } from '../semester.service';

@Injectable({
  providedIn: 'root'
})
export class SemSubjectsService {

  subject: string;
  semester;

  constructor(private db: AngularFirestore,
              private ss: SemesterService) { }

  setsubject(subject){
    this.subject = subject;
  }

  getsemester(){
    this.semester = this.ss.getsem();
    return this.semester;
  }

  getsubject(){
    return this.subject;
  }

  getsubjects(){
    
    return this.db.collection('Semester').doc(this.getsemester()).collection('Subjects').snapshotChanges();
    //console.log(this.db.collection('gpat').doc(this.subject));
  }

  getname()
  {
    this.getsemester();
    return this.db.collection('Semester').doc(this.getsemester()).collection('Subjects').doc(this.subject).snapshotChanges();
  }
}