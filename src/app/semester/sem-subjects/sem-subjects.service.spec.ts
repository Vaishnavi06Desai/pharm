import { TestBed } from '@angular/core/testing';

import { SemSubjectsService } from './sem-subjects.service';

describe('SemSubjectsService', () => {
  let service: SemSubjectsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SemSubjectsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
