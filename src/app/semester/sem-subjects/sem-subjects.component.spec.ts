import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemSubjectsComponent } from './sem-subjects.component';

describe('SemSubjectsComponent', () => {
  let component: SemSubjectsComponent;
  let fixture: ComponentFixture<SemSubjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemSubjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemSubjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
