import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SemesterService {

  semester;

  constructor() { }

  setsem(sem)
  {
    this.semester = sem;
  }

  getsem()
  {
    return this.semester;
  }
}
