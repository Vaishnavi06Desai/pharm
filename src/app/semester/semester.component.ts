import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { SemesterService } from './semester.service';

@Component({
  selector: 'app-semester',
  templateUrl: './semester.component.html',
  styleUrls: ['./semester.component.scss']
})
export class SemesterComponent implements OnInit {

  
  opened: boolean;  
  user: firebase.User;
  constructor(private auth: AuthService,
    private router: Router,
    private ss: SemesterService) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
  }

  logout() {
    this.auth.logout();
  }

  setsem(sem: string)
  {
    this.ss.setsem(sem);
    this.router.navigate(['/semester/semsub']);
  }

}
