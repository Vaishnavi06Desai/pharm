import { TestBed } from '@angular/core/testing';

import { FreeVidsService } from './free-vids.service';

describe('FreeVidsService', () => {
  let service: FreeVidsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FreeVidsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
