import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FreeVidsService } from '../free-vids.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.scss']
})
export class TopicsComponent implements OnInit {

  topics;
  opened: boolean;  
  user: firebase.User;

  constructor(private db: AngularFirestore,
    private auth: AuthService,
    private fv: FreeVidsService,
    private router: Router) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.gettopics();
  }

  gettopics(){
    this.fv.gettopics().subscribe(res => (this.topics = res));
  }

  playvid(vid)
  {
    this.fv.setvid(vid);
    this.router.navigate(['/freevid']);
  }

  logout() {
    this.auth.logout();
  }
}
