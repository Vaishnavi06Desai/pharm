import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FreeVidsService {

  chapter: any;
  vid:any;

  constructor(private db: AngularFirestore) { }

  getchapters(){
    return this.db.collection('Free-videos').snapshotChanges();
  }

  gettopics(){
    return this.db.collection("Free-videos").doc(this.chapter.payload.doc.id).collection("Topics").snapshotChanges();
  }

  setchapter(chapter)
  {
    this.chapter = chapter;
  }

  setvid(vid)
  {
    this.vid = vid;
  }

  getvid()
  {
    return this.vid;
  }
}
