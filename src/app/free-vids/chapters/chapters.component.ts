import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FreeVidsService } from '../free-vids.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-chapters',
  templateUrl: './chapters.component.html',
  styleUrls: ['./chapters.component.scss']
})
export class ChaptersComponent implements OnInit {

  chapters;
  opened: boolean;  
  user: firebase.User;

  constructor(private db: AngularFirestore,
              private auth: AuthService,
              private fv: FreeVidsService,
              private router: Router) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getchapters();
  }

  getchapters()
  {
    this.fv.getchapters().subscribe(res => (this.chapters = res));
  }

  next(chapter)
  {
    this.fv.setchapter(chapter);
    this.router.navigate(['/freetopics']);
  }

  logout() {
    this.auth.logout();
  }
}
