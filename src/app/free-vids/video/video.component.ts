import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FreeVidsService } from '../free-vids.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {

  video:any;

  constructor(private db: AngularFirestore,
    private fv: FreeVidsService,
    private router: Router) { }

  ngOnInit(): void {
    this.getvid();
  }

  getvid(){
    this.video = this.fv.getvid();
  }

}
