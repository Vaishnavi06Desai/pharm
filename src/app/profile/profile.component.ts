import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { ProfileService } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  userProfile;

  constructor(private auth: AuthService, private ps: ProfileService) { }

  getUserProfile(user){
    this.ps.getUserData(user).subscribe(res=> {
     this.userProfile = res;
   });
  }

  
  logout() {
    this.auth.logout();
  }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
      this.getUserProfile(user);
    })

    
  }


}
