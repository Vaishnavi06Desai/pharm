import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor( private afAuth: AngularFireAuth,
    private db: AngularFirestore) { }

    getUserData(user: firebase.User){
      
      return this.db.collection('Users').doc(user.uid).snapshotChanges();
    }
}
