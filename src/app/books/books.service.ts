import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  book;

  constructor(private db: AngularFirestore) { }

  getbooks()
  {
    return this.db.collection('Books').snapshotChanges();
  }

  setbook(data)
  {
    this.book = data;
  }

  getbook()
  {
    return this.book;
  }
}