import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})


export class BooksComponent implements OnInit {

  books;

  opened: boolean;  
  user: firebase.User;
  constructor(private auth: AuthService,
    private router: Router,
    private bs: BooksService) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getbooks();
  }

  getbooks(){
    this.bs.getbooks().subscribe(res => (this.books = res));
  }

  logout() {
    this.auth.logout();
  }

  next(book)
  {
    this.bs.setbook(book);
    this.router.navigate(['/book']);
  }
}
