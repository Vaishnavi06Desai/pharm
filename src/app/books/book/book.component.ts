import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BooksService } from '../books.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  constructor(private bs: BooksService, public sanitizer: DomSanitizer,
              private router: Router ) { }

  book;

  ngOnInit(): void {
    this.getbook();
  }

  getbook(){
    this.book = this.bs.getbook();
  }

}
