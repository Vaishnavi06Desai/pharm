import { TestBed } from '@angular/core/testing';

import { GpatService } from './gpat.service';

describe('GpatService', () => {
  let service: GpatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GpatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
