import { Component, OnInit } from '@angular/core';
import { McqsService } from '../mcqs.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-mcq',
  templateUrl: './mcq.component.html',
  styleUrls: ['./mcq.component.scss']
})
export class McqComponent implements OnInit {

  constructor(private mcqs: McqsService,
              public sanitizer: DomSanitizer) { }

  mcq;
  ngOnInit(): void {
    this.getmcq();
  }

  getmcq()
  {
    this.mcq = this.mcqs.getmcq();
  }

}
