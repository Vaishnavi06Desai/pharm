import { Injectable } from '@angular/core';
import { GpatService } from '../../gpat.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class McqsService {

  subject;
  chapter;

  constructor(private gps: GpatService,
              private db: AngularFirestore) { }

  getsubject()
  {
    this.subject = this.gps.getsubject();
    return this.subject;
  }

  getchapters(subject)
  {
    return this.db.collection('gpat').doc(subject).collection('MCQs').snapshotChanges(); //replace ClinicalPharmacy with this.subject
  }

  setchapter(chapter)
  {
    this.chapter = chapter;
  }

  getmcq()
  {
    return this.chapter; 
  }
}
