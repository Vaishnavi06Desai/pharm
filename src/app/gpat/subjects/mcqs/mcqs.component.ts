import { Component, OnInit } from '@angular/core';
import { McqsService } from './mcqs.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-mcqs',
  templateUrl: './mcqs.component.html',
  styleUrls: ['./mcqs.component.scss']
})
export class McqsComponent implements OnInit {

  subject;
  chapters;
  opened: boolean;  
  user: firebase.User;
  constructor(private mcqs: McqsService,
              private router: Router,
              private db: AngularFirestore,
              private auth: AuthService,) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getchapters();
  }

  logout() {
    this.auth.logout();
  }

  getchapters()
  {
    this.subject =  this.mcqs.getsubject();
    this.mcqs.getchapters(this.subject).subscribe(res => {this.chapters = res;});
  }

  next(chapter)
  {
    this.mcqs.setchapter(chapter);
    this.router.navigate(['/gpat/subjects/mcqs/mcq']);
  }

}
