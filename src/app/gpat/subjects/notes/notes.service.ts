import { Injectable } from '@angular/core';
import { GpatService } from '../../gpat.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  subject;
  chapter;

  constructor(private gps: GpatService,
    private db: AngularFirestore) { }

    getsubject()
    {
      this.subject = this.gps.getsubject();
      return this.subject;
    }
  
    getchapters(subject)
    {
      return this.db.collection('gpat').doc(subject).collection('Notes').snapshotChanges(); //replace ClinicalPharmacy with this.subject
    }
  
    setchapter(chapter)
    {
      this.chapter = chapter;
    }
  
    getnote()
    {
      return this.chapter; 
    }
}
