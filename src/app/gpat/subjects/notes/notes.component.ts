import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { NotesService } from './notes.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {

  subject;
  chapters;
  opened: boolean;  
  user: firebase.User;
  constructor(private ns: NotesService,
              private router: Router,
              private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getchapters();
  }

  logout() {
    this.auth.logout();
  }

  getchapters()
  {
    this.subject =  this.ns.getsubject();
    this.ns.getchapters(this.subject).subscribe(res => {this.chapters = res;});
  }

  next(chapter)
  {
    this.ns.setchapter(chapter);
    this.router.navigate(['/gpat/subjects/notes/note']);
  }

}
