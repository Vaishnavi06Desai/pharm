import { Component, OnInit } from '@angular/core';
import { NotesService } from '../notes.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {

  constructor(private ns: NotesService,
    public sanitizer: DomSanitizer) { }

  note;


  ngOnInit(): void {
    this.getnote();
  }

  getnote()
  {
    this.note = this.ns.getnote();
  }

}
