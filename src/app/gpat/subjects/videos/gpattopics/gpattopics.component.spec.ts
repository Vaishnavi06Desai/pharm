import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpattopicsComponent } from './gpattopics.component';

describe('GpattopicsComponent', () => {
  let component: GpattopicsComponent;
  let fixture: ComponentFixture<GpattopicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpattopicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpattopicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
