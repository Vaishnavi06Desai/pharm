import { Component, OnInit } from '@angular/core';
import { VideosService } from '../../videos.service';

@Component({
  selector: 'app-gpatvideo',
  templateUrl: './gpatvideo.component.html',
  styleUrls: ['./gpatvideo.component.scss']
})
export class GpatvideoComponent implements OnInit {
  video:any;

  constructor(private vs: VideosService) { }

  ngOnInit(): void {
    this.getvid();
  }

  getvid(){
    this.video = this.vs.getvid();
  }

}
