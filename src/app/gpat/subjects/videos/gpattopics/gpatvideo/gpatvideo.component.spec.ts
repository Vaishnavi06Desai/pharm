import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpatvideoComponent } from './gpatvideo.component';

describe('GpatvideoComponent', () => {
  let component: GpatvideoComponent;
  let fixture: ComponentFixture<GpatvideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpatvideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpatvideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
