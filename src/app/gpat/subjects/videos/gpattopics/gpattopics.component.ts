import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { VideosService } from '../videos.service';

@Component({
  selector: 'app-gpattopics',
  templateUrl: './gpattopics.component.html',
  styleUrls: ['./gpattopics.component.scss']
})
export class GpattopicsComponent implements OnInit {

  topics;
  opened: boolean;  
  user: firebase.User;

  constructor(private db: AngularFirestore,
    private auth: AuthService,
    private vs: VideosService,
    private router: Router) { }

    ngOnInit(): void {
      this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
      })
      this.gettopics();
    }

    logout() {
      this.auth.logout();
    }
  
    gettopics(){
      this.vs.gettopics().subscribe(res => (this.topics = res));
    }
  
    playvid(vid)
    {
      this.vs.setvid(vid);
      this.router.navigate(['/gpat/subjects/videos/topics/video']);
    }
  
}
