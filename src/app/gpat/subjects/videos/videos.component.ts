import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { VideosService } from './videos.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {

  chapters;
  opened: boolean;  
  user: firebase.User;

  constructor(private db: AngularFirestore,
    private auth: AuthService,
    private vs: VideosService,
    private router: Router) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getchapters();
  }

  logout() {
    this.auth.logout();
  }

  getchapters()
  {
    this.vs.getchapters(this.vs.getsubject()).subscribe(res => (this.chapters = res));
  }

  next(chapter)
  {
    this.vs.setchapter(chapter);
    this.router.navigate(['/gpat/subjects/videos/topics']);
  }

}
