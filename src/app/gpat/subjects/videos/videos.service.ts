import { Injectable } from '@angular/core';
import { GpatService } from '../../gpat.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  subject;
  chapter: any;
  vid:any;

  constructor(private gps: GpatService,
    private db: AngularFirestore) { }

    getsubject()
    {
      this.subject = this.gps.getsubject();
      return this.subject;
    }

    getchapters(subject){
      return this.db.collection('gpat').doc(subject).collection('Videos').snapshotChanges(); //replace ClinicalPharmacy with this.subject
    }

    gettopics(){
      return this.db.collection('gpat').doc(this.subject).collection('Videos').doc(this.chapter.payload.doc.id).collection("Topics").snapshotChanges(); //replace ClinicalPharmacy with this.subject
    }
  
    setchapter(chapter)
    {
      this.chapter = chapter;
    }
  
    setvid(vid)
    {
      this.vid = vid;
    }
  
    getvid()
    {
      return this.vid;
    }
}
