import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { GpatService } from '../gpat.service';



@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  subject: String = "none";
  name;

  constructor(private db: AngularFirestore,
    private auth: AuthService,
    private gps: GpatService,
    private router: Router) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getname();
  }


  logout() {
    this.auth.logout();
  }

  mcqs()
  {
    this.router.navigate(['/gpat/subjects/mcqs']);
  }

  notes()
  {
    this.router.navigate(['/gpat/subjects/notes']);
  }

  videos()
  {
    this.router.navigate(['/gpat/subjects/videos']);
  }

  getname()
  {
    this.gps.getname(this.gps.getsubject()).subscribe(res => {this.name = res;});
  }


}
