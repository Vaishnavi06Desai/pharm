import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { GpatService } from './gpat.service';

@Component({
  selector: 'app-gpat',
  templateUrl: './gpat.component.html',
  styleUrls: ['./gpat.component.scss']
})
export class GpatComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  constructor(private auth: AuthService,
    private router: Router,
    private gps: GpatService) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
  }

  setsubject(subject: string)
  {
    this.gps.setsubject(subject);
    this.router.navigate(['/gpat/subjects']);
  }

  logout() {
    this.auth.logout();
  }

}
