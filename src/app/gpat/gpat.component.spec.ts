import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpatComponent } from './gpat.component';

describe('GpatComponent', () => {
  let component: GpatComponent;
  let fixture: ComponentFixture<GpatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
