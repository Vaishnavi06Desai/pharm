import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class GpatService {

  subject: string;

  constructor(private db: AngularFirestore) { }

  setsubject(subject){
    this.subject = subject;
  }

  getsubject(){
    return this.subject;
  }

  getname(subject){
    return this.db.collection('gpat').doc(subject).snapshotChanges();
    //console.log(this.db.collection('gpat').doc(this.subject));
  }
}
