import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { HostListener } from '@angular/core';
import {Location} from '@angular/common';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  slideIndex: number;
  opened: boolean;  
  user: firebase.User;
  count: number = 0;
  images:any;

  constructor(private auth: AuthService,
    private location: Location,
    private router: Router,
    private db: AngularFirestore) { 
    this.slideIndex = 0;
  }

  ngOnInit(): void {
    this.auth.getUserState()
      .subscribe( user => {
        this.user = user;
      })
      this.getimages();
      //this.showSlides();
      
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.location.go('/home');
    console.log('Back button pressed');
  }

  logout() {
    this.auth.logout();
  }

  getimages()
  {
      return this.db.collection('images').snapshotChanges().subscribe(res => {this.images = res; console.log(res)});
  }

  /*onDragEnded(event: CdkDragEnd) {
    let dn = <HTMLElement><any>document.getElementById("dragnav");
    //dn.style.left = "0px";
    this.offset = { ...(<any>event.source._dragRef)._passiveTransform };

    this.position.x = this.initialPosition.x;
    this.position.y = this.initialPosition.y;

    console.log(this.position, this.initialPosition, this.offset, dn.style.left);
  }*/

  showSlides() {
    var i;
    let slides = <HTMLElement[]><any>document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    this.slideIndex++;
    if (this.slideIndex > slides.length) {this.slideIndex = 1}
    slides[this.slideIndex-1].style.display = "block";
    setTimeout(() => {this.showSlides()}, 2000);

  }

}
