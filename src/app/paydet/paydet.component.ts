import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-paydet',
  templateUrl: './paydet.component.html',
  styleUrls: ['./paydet.component.scss']
})
export class PaydetComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;

  constructor(private auth: AuthService) { }

  logout() {
    this.auth.logout();
  }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })

    
  }
}
