import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaydetsComponent } from './paydets.component';

describe('PaydetsComponent', () => {
  let component: PaydetsComponent;
  let fixture: ComponentFixture<PaydetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaydetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaydetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
