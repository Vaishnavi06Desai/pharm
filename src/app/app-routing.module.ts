import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { HomeComponent } from './home/home.component';
import { GpatComponent } from './gpat/gpat.component';
import { ChaptersComponent } from './free-vids/chapters/chapters.component';
import { TopicsComponent } from './free-vids/topics/topics.component';
import { VideoComponent } from './free-vids/video/video.component';
import { MemchartsComponent } from './memcharts/memcharts.component';
import { BooksComponent } from './books/books/books.component';
import { BookComponent } from './books/book/book.component';
import { SubjectsComponent } from './gpat/subjects/subjects.component';
import { VideosComponent } from './gpat/subjects/videos/videos.component';
import { McqsComponent } from './gpat/subjects/mcqs/mcqs.component';
import { NotesComponent } from './gpat/subjects/notes/notes.component';
import { GpattopicsComponent } from './gpat/subjects/videos/gpattopics/gpattopics.component';
import { GpatvideoComponent } from './gpat/subjects/videos/gpattopics/gpatvideo/gpatvideo.component';
import { NoteComponent } from './gpat/subjects/notes/note/note.component';
import { McqComponent } from './gpat/subjects/mcqs/mcq/mcq.component';
import { SemesterComponent } from './semester/semester.component';
import { SemSubjectsComponent } from './semester/sem-subjects/sem-subjects.component';
import { SemSubOptsComponent } from './semester/sem-subjects/sem-sub-opts/sem-sub-opts.component';
import { SemVideosComponent } from './semester/sem-subjects/sem-sub-opts/sem-videos/sem-videos.component';
import { SemPastPapersComponent } from './semester/sem-subjects/sem-sub-opts/sem-past-papers/sem-past-papers.component';
import { SemNotesComponent } from './semester/sem-subjects/sem-sub-opts/sem-notes/sem-notes.component';
import { SemTopicsComponent } from './semester/sem-subjects/sem-sub-opts/sem-videos/sem-topics/sem-topics.component';
import { SemVideoComponent } from './semester/sem-subjects/sem-sub-opts/sem-videos/sem-topics/sem-video/sem-video.component';
import { SemNoteComponent } from './semester/sem-subjects/sem-sub-opts/sem-notes/sem-notes/sem-note/sem-note.component';
import { SemPastPaperComponent } from './semester/sem-subjects/sem-sub-opts/sem-past-papers/sem-past-paper/sem-past-paper.component';
import { TestSeriesComponent } from './test-series/test-series.component';
import { ProfileComponent } from './profile/profile.component';
import { SupportComponent } from './support/support.component';
import { CertificationComponent } from './certification/certification.component';
import { FeeComponent } from './fee/fee.component';
import { FeesComponent } from './fees/fees.component';
import { PaydetComponent } from './paydet/paydet.component';
import { PaydetsComponent } from './paydets/paydets.component';


const routes: Routes = [
  {
    path :'login',
    component: LoginComponent
  },
  {
    path :'register',
    component: RegistrationComponent
  },
  {
    path :'home',
    component: HomeComponent
  },
  {
    path :'gpat',
    component: GpatComponent
  },
  {
    path :'freechapters',
    component: ChaptersComponent
  },
  {
    path :'freetopics',
    component: TopicsComponent
  },
  {
    path :'freevid',
    component: VideoComponent
  },
  {
    path :'memcharts',
    component: MemchartsComponent
  },
  {
    path :'books',
    component: BooksComponent
  },
  {
    path :'book',
    component: BookComponent
  },
  {
    path:'gpat/subjects',
    component: SubjectsComponent
  },
  {
    path:'gpat/subjects/videos',
    component: VideosComponent
  },
  {
    path:'gpat/subjects/mcqs',
    component: McqsComponent
  },
  {
    path:'gpat/subjects/notes',
    component: NotesComponent
  },
  {
    path:'gpat/subjects/videos/topics',
    component: GpattopicsComponent
  },
  {
    path:'gpat/subjects/videos/topics/video',
    component: GpatvideoComponent
  },
  {
    path:'gpat/subjects/notes/note',
    component: NoteComponent
  },
  {
    path:'gpat/subjects/mcqs/mcq',
    component: McqComponent
  },
  {
    path:'semester',
    component: SemesterComponent
  },
  {
    path:'semester/semsub',
    component: SemSubjectsComponent
  },
  {
    path: 'semester/semsub/options',
    component: SemSubOptsComponent
  },
  {
    path:'semester/semsub/options/videos',
    component: SemVideosComponent
  },
  {
    path:'semester/semsub/options/papers',
    component: SemPastPapersComponent
  },
  {
    path:'semester/semsub/options/notes',
    component: SemNotesComponent
  },
  {
    path:'semester/semsub/options/videos/topics',
    component: SemTopicsComponent
  },
  {
    path:'semester/semsub/options/videos/topics/video',
    component: SemVideoComponent
  },
  {
    path:'semester/semsub/options/notes/note',
    component: SemNoteComponent
  },
  {
    path:'semester/semsub/options/papers/paper',
    component: SemPastPaperComponent
  },
  {
    path:'testseries',
    component: TestSeriesComponent
  },
  {
    path:'profile',
    component: ProfileComponent
  },
  {
    path:'support',
    component: SupportComponent
  },
  {
    path:'certification',
    component: CertificationComponent
  },
  {
    path:'feestructure',
    component: FeeComponent
  },
  {
    path:'feestructuresem',
    component: FeesComponent
  },
  {
    path:'paymentdetails',
    component: PaydetComponent
  },
  {
    path:'paymentdetailssem',
    component: PaydetsComponent
  },
  {
    path :'',
    redirectTo: '/login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
