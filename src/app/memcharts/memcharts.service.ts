import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class MemchartsService {

  constructor(private db: AngularFirestore) { }

  getcharts(){
    return this.db.collection('MemoryCharts').snapshotChanges();
  }
}
