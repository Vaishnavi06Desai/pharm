import { Component, OnInit } from '@angular/core';
import { MemchartsService } from './memcharts.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-memcharts',
  templateUrl: './memcharts.component.html',
  styleUrls: ['./memcharts.component.scss']
})
export class MemchartsComponent implements OnInit {

  opened: boolean;  
  user: firebase.User;
  chartsr;
  charts = [];
  chart: any;
  constructor(private auth: AuthService,
    private router: Router,
    private ms: MemchartsService) { }

  ngOnInit(): void {
    this.auth.getUserState()
    .subscribe( user => {
      this.user = user;
    })
    this.getcharts();
  }

  logout() {
    this.auth.logout();
  }

  toggle(chart)
  {
    this.chart = chart;
    let charts = <HTMLElement[]><any>document.getElementsByClassName("fullimg");
    charts[0].style.display = "block";
  }

  toggleback()
  {
    let charts = <HTMLElement[]><any>document.getElementsByClassName("fullimg");
    charts[0].style.display = "none";
  }

  getcharts(){
    this.ms.getcharts().subscribe(res => {this.chartsr = res; this.chart = res[0]; for(let i = res.length - 1; i >= 0; i--){this.charts[i] = res[(res.length - 1) - (i)]; }});
  }

}
