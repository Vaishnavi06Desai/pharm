import { TestBed } from '@angular/core/testing';

import { MemchartsService } from './memcharts.service';

describe('MemchartsService', () => {
  let service: MemchartsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MemchartsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
