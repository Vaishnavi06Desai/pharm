import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemchartsComponent } from './memcharts.component';

describe('MemchartsComponent', () => {
  let component: MemchartsComponent;
  let fixture: ComponentFixture<MemchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
