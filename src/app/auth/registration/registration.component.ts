import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {


  authError:any;

  constructor(private auth:AuthService) { }

  ngOnInit(): void {
    this.auth.eventAuthError$.subscribe(data => {
      this.authError=data
    })
  }

  valid(frm){
    var flag = 'true';
     
    //console.log(frm.value.firstName);
    
    if (frm.value.firstName == "" || !(/^[A-Za-z]+$/.test(frm.value.firstName)))                                  
    { 
        window.alert("Please enter a valid first name."); 
        frm.firstName.focus(); 
        flag = 'false'; 
    } 
   
    if (frm.value.lastName == "" || !(/^[a-zA-Z]/.test(frm.value.lastName)))                               
    { 
        window.alert("Please enter a valid last name."); 
        frm.lastName.focus(); 
        flag = 'false'; 
    } 
 
    if (frm.value.phnum == "" || !(/^\d{10}$/.test(frm.value.phnum)) )                           
    { 
        window.alert("Please enter a valid phone number."); 
        frm.phnum.focus(); 
        flag = 'false'; 
    } 
   
    if (frm.value.email == "")                                   
    { 
        window.alert("Please enter a valid e-mail address."); 
        frm.email.focus(); 
        flag = 'false'; 
    } 
    
    if (frm.value.college == "" || !(/^[a-zA-Z]/.test(frm.value.college)))                               
    { 
        window.alert("Please enter a valid college."); 
        frm.college.focus(); 
        flag = 'false'; 
    } 
    if (frm.value.city == "" || !(/^[a-zA-Z]/.test(frm.value.city)))                               
    { 
        window.alert("Please enter a valid city."); 
        frm.city.focus(); 
        flag = 'false'; 
    } 
    if (frm.value.pincode == "" || !((/^\d{6}$/.test(frm.value.pincode))) )                              
    { 
        window.alert("Please enter a valid pincode."); 
        frm.pincode.focus(); 
        flag = 'false'; 
    } 
   
    
    if (frm.value.password == "")                        
    { 
        window.alert("Please enter a your password"); 
        frm.password.focus(); 
        flag = 'false';  
    } 
 
     if (flag == 'true') {
       this.createUser(frm);
     }
   }
 

  createUser(frm){
      this.auth.createUser(frm.value);
  }

}
